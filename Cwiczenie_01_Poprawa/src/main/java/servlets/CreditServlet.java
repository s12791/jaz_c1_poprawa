package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import credit.Credit;
import credit.CreditInstallment;

@WebServlet("/credit")
public class CreditServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		int loanAmount;
		int installmentLoanNumber;
		float percentage;
		int fixedCharge;
		boolean fixedRates;
		
		try{
			loanAmount = Integer.parseInt(request.getParameter("amount"));
			installmentLoanNumber = Integer.parseInt(request.getParameter("installments"));
			percentage = Float.parseFloat(request.getParameter("percentage"));
			percentage = percentage / 100f;
			fixedCharge = Integer.parseInt(request.getParameter("fixedFee"));
			fixedRates = Boolean.parseBoolean(request.getParameter("fixedRates"));
		}catch (NumberFormatException e){
			response.sendRedirect("/");
			return;
		}
		if (loanAmount <= 0 || installmentLoanNumber <= 0 || percentage < 0 || fixedCharge < 0){
			response.sendRedirect("/");
			return;
		}
		
			response.setContentType("text/html");
			response.getWriter().println("<table border='1'>");
			response.getWriter().println("<tr><td>Installment Number</td><td>Installment to pay</td><td>Remaining Amount</td><td>Interest Amount</td><td>Capital quota</td> <td>Fixed Charge</td></tr>");
			
			Credit credit = new Credit();
			
			credit.setAmount(loanAmount);
			credit.setFixedFee(fixedCharge);
			credit.setInstallments(installmentLoanNumber);
			credit.setPercentage(percentage);
			credit.setFixedRates(fixedRates);
			
			List<CreditInstallment> installmentsList = credit.getInstallmentsList();
			for(int i=0; i<installmentsList.size(); i++){
				CreditInstallment c = installmentsList.get(i);
				response.getWriter().println("<tr><td>" + Integer.toString(i+1) +
						"</td><td>" + Double.toString(c.getFullInstallment()) + "</td><td> "
						+ Double.toString(c.getRestOfCapital()) + " </td><td>" + Double.toString(c.getInterest())
						+ "</td> <td>" + Double.toString(c.getCapitalRate()) + "</td><td>"
						+ Double.toString(c.getFixedFee()) + "</td></tr>");
				
			}
		
	}
}
