package credit;

public class CreditInstallment {
	private double fullInstallment;
	private double capitalRate;
	private double restOfCapital;
	private double interest;
	private double fixedFee;
	
	public CreditInstallment(double installment, double installmentCapital, double amount, double interest, double fee){
		this.fullInstallment = installment;
		this.capitalRate = installmentCapital;
		this.restOfCapital = amount;
		this.interest = interest;
		this.fixedFee = fee;
	}
	public double getFullInstallment() {
		return fullInstallment;
	}
	public double getCapitalRate() {
		return capitalRate;
	}
	public double getRestOfCapital() {
		return restOfCapital;
	}
	public double getInterest() {
		return interest;
	}
	public double getFixedFee() {
		return fixedFee;
	}
	
	
}
